<?php
/**
 * Template part for displaying page content in blog
 *
 * @link https://developer.wordpress.org/themes/basics/template-hierarchy/
 *
 * @package allflex
 */

?>

<section class="section-hero-slider">
        <div class="one-image" style="background-image:url(<?php the_post_thumbnail_url(); ?>)">
            <div class="container d-flex flex-column align-items-center justify-content-center">
                <div class="row">
                    <?php 
                    if ( is_singular() ) :
                        the_title( '<h1 class="entry-title">', '</h1>' );
						echo '<h5 class="entry-subtitle">'.get_field('blog_subtitle').'</h5>';
                    else :
                        the_title( '<h2 class="entry-title"><a href="' . esc_url( get_permalink() ) . '" rel="bookmark">', '</a></h2>' );
                    endif; ?>   
                    <?php 
                    the_post_navigation(    
                        array(
                            'prev_text' => '<span class="nav-subtitle"><span class="icomoon icomoon-arrow-left" aria-hidden="true"></span></span>',
                            'next_text' => '<span class="nav-subtitle"><span class="icomoon icomoon-arrow-right" aria-hidden="true"></span></span>',
                        )
                    );
                    ?>
                </div>
            </div>
        </div>
        <div class="scroll-to-bottom">
            <button class="btn-scroll-to btn-scroll-to-bottom" data-scrollto=".section-our-partners" aria-label="Scroll to next section" title="Scroll to next section">
                <span class="fa fa-chevron-down" aria-hidden="true"></span>
            </button>
        </div>
    </section>
<article id="post-<?php the_ID(); ?>" <?php post_class(); ?>>
    <div class="container">
        <?php
            if ( function_exists('yoast_breadcrumb') ) {
              yoast_breadcrumb( '<p id="breadcrumbs">','</p>' );
            }
        ?>
         <div class="row">
            <?php if( get_field('author_image') ){ ?>
                <div class="col-lg-1">
                        <img src="<?php the_field('author_image'); ?>" />
                </div>
                <div class="col-lg-11">
            <?php } else { ?>
                <div class="col-lg-12">
           <?php } ?>
                <header class="entry-header">
                    <?php
                    if ( 'post' === get_post_type() ) :
                        ?>
                        <div class="entry-meta">                
                            <?php
                            allflex_posted_by();
                            echo '<br>';
                            allflex_posted_on(); ?>
                            <?php if(get_field('entry_title')){
                                echo '<h2>';
                                echo the_field('entry_title');
                                echo '</h2>';
                            } ?>
                        </div><!-- .entry-meta -->
                    <?php endif; ?>
                </header><!-- .entry-header -->
                <div class="entry-content">
                    <?php
                    the_content(
                        sprintf(
                            wp_kses(
                                /* translators: %s: Name of current post. Only visible to screen readers */
                                __( 'Continue reading<span class="screen-reader-text"> "%s"</span>', 'allflex' ),
                                array(
                                    'span' => array(
                                        'class' => array(),
                                    ),
                                )
                            ),
                            wp_kses_post( get_the_title() )
                        )
                    );

                    wp_link_pages(
                        array(
                            'before' => '<div class="page-links">' . esc_html__( 'Pages:', 'allflex' ),
                            'after'  => '</div>',
                        )
                    );
                    ?>
                </div><!-- .entry-content -->
              <!--   <footer class="entry-footer">
                    <?php allflex_entry_footer(); ?>
                </footer>.entry-footer -->
            </div>
        </div>
        <?php $posts = get_field('related_posts');
        if( $posts ): ?>
            <h2 class="entry-title after-blue">Tevi varētu interesēt šie raksti</h2>
            <div class="row row-posts justify-content-center">
                <?php
                /* Start the Loop */
                 foreach( $posts as $post): // variable must be called $post (IMPORTANT) ?>
                    <?php setup_postdata($post); ?>
                        <div class="col-lg-4 col-md-6 blog-item blog-item-post">
                            <a href="<?php the_permalink(); ?>">
                                <div class="post-thumbnail">
                                        <?php the_post_thumbnail( 'medium' ); ?>
                                    <footer class="entry-footer">
                                        <h4 class="entry-title"><?php the_title(); ?></h4>
                                        <p class="entry-excerpt">
                                            <?php the_excerpt(); ?>
                                        </p>
                                        <span class="btn btn-blue-text">
                                        Lasīt vairāk</span>
                                    </footer>
                                </div>
                            </a>
                        </div>
                <?php endforeach; ?>
            </div>
        <?php wp_reset_postdata(); // IMPORTANT - reset the $post object so the rest of the page works correctly ?>
        <?php endif; ?>
    </div>
</article><!-- #post-<?php the_ID(); ?> -->