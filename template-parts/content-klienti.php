<?php
/**
 * Template part for displaying page content in blog
 *
 * @link https://developer.wordpress.org/themes/basics/template-hierarchy/
 *
 * @package allflex
 */

?>

<main id="primary" class="site-main">
    <section class="section-hero-slider hero-half"> 
                <div class="half-bg">
                    <div class="half-image col-md-7" style="background-image:url(<?php the_post_thumbnail_url(); ?>)"></div>
                    <div class="col-md-5 bg-white"></div>
                </div>
                <div class="container">
                    <div class="offset-md-8 col-md-4">
                    <?php the_field('header_text'); ?>
                    </div>
                </div>  
            
        
            <div class="scroll-to-bottom">
                <button class="btn-scroll-to btn-scroll-to-bottom" data-scrollto=".section-our-partners" aria-label="Scroll to next section" title="Scroll to next section">
                    <span class="fa fa-chevron-down" aria-hidden="true"></span>
                </button>
            </div>
        </section>
                <section class="section-textual">
                    <div class="container">
                        <?php
                            if ( function_exists('yoast_breadcrumb') ) {
                              yoast_breadcrumb( '<p id="breadcrumbs">','</p>' );
                            }
                        ?>
                        <?php if( get_field('quotation') ) { ?>
                        <section class="section-testimonial">
                            <div class="container">
                                <div class="cols">
                                    <div class="col-icon">
                                        <span class="sprite sprite-quote" aria-hidden="true"></span>
                                    </div>
                                    <div class="col-content">
                                        <blockquote class="blockquote">
                                            <p><?php the_field('quotation'); ?></p>
                                            <cite class="cite">
                                                <span class="entry-text"><?php the_field('quote_author'); ?></span>
                                            </cite>
                                        </blockquote>
                                    </div>
                                </div>
                            </div>
                        </section>
                        <?php } ?>
                        <?php if( get_field('challange') && get_field('used_system') && get_field('benefits') ) { ?>
                            <section class="section-challenges">
                                <div class="container">
                                    <div class="row">
                                        <div class="col-lg-4 aos-init aos-animate" data-aos="fade-in" data-aos-delay="0" data-aos-duration="1000">
                                            <div class="item" style="background: #005587">      
                                                <figure class="entry-image">
                                                    <img src="https://www.allflex.global/wp-content/uploads/2019/11/icon-1.png" alt="">
                                                </figure>
                                                <h3 class="entry-title" style="color: #fff">Izaicinājums</h3>
                                                <div class="entry-text" style="color: #fff">
                                                    <?php the_field('challange'); ?>
                                                </div>
                                            </div>
                                        </div>
                                        <div class="col-lg-4 aos-init aos-animate" data-aos="fade-in" data-aos-delay="200" data-aos-duration="1000">
                                            <div class="item" style="background: #009cde">
                                                <figure class="entry-image">
                                                    <img src="https://www.allflex.global/wp-content/uploads/2019/11/icon-2.png" alt="">
                                                </figure>
                                                <h3 class="entry-title" style="color: #ffffff">Risinājums</h3>
                                                <div class="entry-text" style="color: #efefef">
                                                    <?php the_field('used_system'); ?>
                                                </div>
                                            </div>
                                        </div>
                                        <div class="col-lg-4 aos-init aos-animate" data-aos="fade-in" data-aos-delay="400" data-aos-duration="1000">
                                            <div class="item" style="background: #5c3161">      
                                                <figure class="entry-image">
                                                    <img src="https://www.allflex.global/wp-content/uploads/2019/11/icon-3.png" alt="">
                                                </figure>
                                                <h3 class="entry-title" style="color: #fff">Ieguvumi</h3>
                                                <div class="entry-text" style="color: #fff">
                                                <?php the_field('benefits'); ?>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </section>
                        <?php } ?>
                        <div class="row">
                            <div class="col-lg-6">
                                <header class="entry-header">
                                    <h2 class="entry-title">Īsumā</h2>
                                    <div class="entry-text"><?php the_field('main_text'); ?></div>
                                </header>
                                <?php if (have_rows('solution_images')): ?>
                                    <div class="images">
                                        <?php while (have_rows('solution_images')) : the_row(); ?>  
                                            <div class="col-md-6 col-image">
                                                <figure class="entry-image">
                                                    <img src="<?php the_sub_field('solution_image') ?>" alt="">
                                                </figure>
                                            </div>
                                        <?php endwhile; ?>
                                    </div>
                                <?php endif; ?>
                            </div>
                            <div class="col-lg-6">
                                <div class="entry-content">
                                    <?php
                                    the_content();

                                    wp_link_pages(
                                        array(
                                            'before' => '<div class="page-links">' . esc_html__( 'Pages:', 'allflex' ),
                                            'after'  => '</div>',
                                        )
                                    );
                                    ?>
                                </div>
                                <div class="thumb-up-message">
                                    <img src="https://www.allflex.global/wp-content/uploads/2020/02/thankyou-icon.png">
                                    <p>Vairāk informācijas sazinoties ar mums.</p>
                                </div>
                            </div>
                        </div>
                    </div>
                </section><!-- .section-textual -->
            <?php if (have_rows('pdf_brouchures')): ?>
                <section class="section-pdf">
                    <div class="container">
                        <h3 class="large-title aos-init aos-animate" data-aos="fade-in">Lejupielādē klienta stāstu</h3>
                        <div class="download-wrapper">
                            <?php while (have_rows('pdf_brouchures')) : the_row(); 
                                $brochure = get_sub_field('brochure');  ?>
                                <a href="<?php echo $brochure['url']; ?>" class="btn btn-blue" title="sensehub-dairy-brochure" target="_blank">
                                <span class="pdf-icon">
                                    <img src="<?php echo get_stylesheet_directory_uri() . '/img/download-icon.png' ?>" alt="Download">            
                                </span>
                                <span>
                                    <?php echo $brochure['title']; ?>
                                </span>
                                <span class="btn btn-blue-text"></span>
                                </a>
                           <?php endwhile; ?>     
                        </div>
                    </div>
                </section><!-- .section-pdf -->
            <?php endif; ?>
            <?php if ( get_field('youtube_video') ): ?>
                <section class="section-video-modal parallax-item">
                    <div class="container">
                        <a href="<?php the_field('youtube_video'); ?>" data-fancybox="">
                            <span class="icon-play-l"></span>   
                            <figure class="entry-image">
                                <img src="<?php the_field('cover_img'); ?>" alt="">
                            </figure>
                        </a>
                    </div>
                </section><!-- .section-video-modal -->
            <?php endif; ?>
            <?php $posts = get_field('related_posts');
            if( $posts ): ?>
                <section class="section-solutions">
                    <div class="container"> 
                        <h2 class="entry-title after-blue">Saistīta informācija</h2>
                        <div class="row entry-qs-slider entry-qs-slider-solutions">
                            <?php foreach( $posts as $post): // variable must be called $post (IMPORTANT) ?>
                                <?php setup_postdata($post); ?>
                                    <div class="col-md-3 slide">
                                        <a href="<?php the_permalink(); ?>">
                                            <div class="item item-post">
                                                <figure class="entry-image">
                                                    <?php the_post_thumbnail( 'medium' ); ?>
                                                </figure>
                                                <footer class="entry-footer">
                                                    <h3 class="entry-title"><?php the_title(); ?></h3>
                                                    <span class="btn btn-blue-text">
                                                    Lasīt vairāk</span>
                                                </footer>
                                            </div>
                                        </a>
                                    </div>
                            <?php endforeach; ?>
                        </div>
                    </div>
                </section><!-- .section-solutions -->
                <?php wp_reset_postdata(); // IMPORTANT - reset the $post object so the rest of the page works correctly ?>
            <?php endif; ?>
    </main><!-- #main -->