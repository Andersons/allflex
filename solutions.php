<?php 
/** 
* Template Name: Solutions page 
* The template for displaying Solutions page.
*
*
*@package allflex
*/ 
get_header();
?>

<main id="primary" class="site-main">
		<?php
		while ( have_posts() ) :
			the_post(); ?>
				<section class="section-textual">
					<div class="container">
						<?php
							if ( function_exists('yoast_breadcrumb') ) {
							  yoast_breadcrumb( '<p id="breadcrumbs">','</p>' );
							}
						?>
						<div class="row">
							<div class="col-lg-6">
								<header class="entry-header">
									<h2 class="entry-title"><?php the_title(); ?></h2>
									<div class="entry-text"><?php the_field('main_text'); ?></div>
								</header>
								<?php if (have_rows('solution_images')): ?>
									<div class="images">
										<?php while (have_rows('solution_images')) : the_row(); ?>	
											<div class="col-6 col-image">
												<figure class="entry-image">
													<img src="<?php the_sub_field('solution_image') ?>" alt="">
												</figure>
											</div>
										<?php endwhile; ?>
									</div>
								<?php endif; ?>
							</div>
							<div class="col-lg-6">
								<div class="entry-content">
									<?php
									the_content();

									wp_link_pages(
										array(
											'before' => '<div class="page-links">' . esc_html__( 'Pages:', 'allflex' ),
											'after'  => '</div>',
										)
									);
									?>
								</div>
								<div class="thumb-up-message">
									<img src="<?php echo get_stylesheet_directory_uri() ?>/img/call.png">
									<p><a href="/kontakti"><u>Sazinies ar mums</u></a>, lai saņemtu detalizētāku informāciju!</p>
								</div>
							</div>
						</div>
					</div>
				</section><!-- .section-textual -->
			<?php if (have_rows('pdf_brouchures')): ?>
				<section class="section-pdf">
				    <div class="container">
				      	<h3 class="large-title aos-init aos-animate" data-aos="fade-in">Lejupielādē bukletus</h3>
				      	<div class="download-wrapper">
				      		<?php while (have_rows('pdf_brouchures')) : the_row(); 
				      			$brochure = get_sub_field('brochure');	?>
					            <a href="<?php echo $brochure['url']; ?>" class="btn btn-blue" title="sensehub-dairy-brochure" target="_blank">
					            <span class="pdf-icon">
					            	<img src="<?php echo get_stylesheet_directory_uri() . '/img/download-icon.png' ?>" alt="Download">            
					            </span>
					            <span>
					            	<?php echo $brochure['title']; ?>
					            </span>
					            <span class="btn btn-blue-text"></span>
					          	</a>
				           <?php endwhile; ?>     
				        </div>
				    </div>
				</section><!-- .section-pdf -->
			<?php endif; ?>
			<?php if ( get_field('youtube_video') ): ?>
				<section class="section-video-modal parallax-item">
					<div class="container">
						<a href="<?php the_field('youtube_video'); ?>" data-fancybox="">
							<span class="icon-play-l"></span>	
							<figure class="entry-image">
								<img src="<?php the_field('cover_img'); ?>" alt="">
							</figure>
						</a>
					</div>
				</section><!-- .section-video-modal -->
			<?php endif; ?>
			<?php $posts = get_field('related_posts');
			if( $posts ): ?>
    			<section class="section-solutions">
					<div class="container">	
						<h2 class="entry-title after-blue">Saistīta informācija</h2>
						<div class="row entry-qs-slider entry-qs-slider-solutions">
						    <?php foreach( $posts as $post): // variable must be called $post (IMPORTANT) ?>
						        <?php setup_postdata($post); ?>
						       		<div class="col-md-3 slide">
							            <a href="<?php the_permalink(); ?>">
							            	<div class="item item-post">
							            		<figure class="entry-image">
							            			<?php the_post_thumbnail( 'medium' ); ?>
							            		</figure>
							            		<footer class="entry-footer">
							            			<h3 class="entry-title"><?php the_title(); ?></h3>
							            			<div class="entry-text">
							            				<?php the_excerpt(); ?>
							            			</div>
							            			<span class="btn btn-blue-text">
													Lasīt vairāk</span>
							            		</footer>
							            	</div>
							            </a>
						       		</div>
						    <?php endforeach; ?>
    					</div>
					</div>
				</section><!-- .section-solutions -->
			    <?php wp_reset_postdata(); // IMPORTANT - reset the $post object so the rest of the page works correctly ?>
			<?php endif; ?>

		<?php endwhile; // End of the loop.
		?>
	</main><!-- #main -->

<?php
get_footer();