<?php
/**
 * The main template file
 *
 * This is the most generic template file in a WordPress theme
 * and one of the two required files for a theme (the other being style.css).
 * It is used to display a page when nothing more specific matches a query.
 * E.g., it puts together the home page when no home.php file exists.
 *
 * @link https://developer.wordpress.org/themes/basics/template-hierarchy/
 *
 * @package allflex
 */

get_header();
?>

	<main id="primary" class="site-main">
		<section class="section-hero-slider">
			<div class="one-image" style="background-image:url(<?php the_post_thumbnail_url(); ?>)">
				<div class="container d-flex flex-column align-items-center justify-content-center">
					<div class="row align-items-center">
						<header class="page-header text-center">
							<h1><?php single_post_title(); ?></h1>
							<h2>
								Noderīgi un izglītojoši raksti par piena un liellopu saimniecības pārvaldību un inovatīviem nozares risinājumiem
							</h2>
						</header><!-- .page-header -->
					</div>
				</div>
			</div>
			<div class="scroll-to-bottom">
				<button class="btn-scroll-to btn-scroll-to-bottom" data-scrollto=".section-our-partners" aria-label="Scroll to next section" title="Scroll to next section">
					<span class="fa fa-chevron-down" aria-hidden="true"></span>
				</button>
			</div>
		</section>
		<div class="container">
			<?php
				if ( function_exists('yoast_breadcrumb') ) {
				  yoast_breadcrumb( '<p id="breadcrumbs">','</p>' );
				}
			?>
			<?php echo do_shortcode('[pgaf_post_filter include_cat_child="false"]'); ?>
	

	</main><!-- #main -->

<?php

get_footer();
