<?php
/**
 * The template for displaying the footer
 *
 * Contains the closing of the #content div and all content after.
 *
 * @link https://developer.wordpress.org/themes/basics/template-files/#template-partials
 *
 * @package allflex
 */

?>

	<footer id="colophon" class="site-footer">
		<div class="site-info container">
			<div class="row">
				<div class="col-lg-3 footer-image">
						<span class="text-body d-block mb-2 font-weight-bold">Oficiālais izplatītājs Latvijā</span>
						<a href="http://futurefarm.lv" target="_blank"><img src="<?php echo get_stylesheet_directory_uri(); ?>/img/FF_logo.png"></a>
				</div>
				<div class="col-lg-9">
					<div class="row pt-4">
						<?php get_sidebar(); ?>
					</div>
					<div class="row row-copyright">
						<span>Copyright © 2020 SCR Engineers Ltd. All rights reserved.</span>
					</div>
				</div>
			</div>
			
			
				
		</div><!-- .site-info -->
	</footer><!-- #colophon -->
</div><!-- #page -->
  <script src="https://unpkg.com/aos@2.3.1/dist/aos.js"></script>
  <script>
  AOS.init();
</script>
<script
  src="https://code.jquery.com/jquery-3.5.1.min.js"
  integrity="sha256-9/aliU8dGd2tb6OSsuzixeV4y/faTqgFtohetphbbj0="
  crossorigin="anonymous"></script>

<?php wp_footer(); ?>
<button class="btn-scroll-to btn-scroll-to-top" data-scrollto=".site-wrap" aria-label="Scroll to Top" title="Scroll to Top"></button>
</body>
</html>
