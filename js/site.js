var $ = jQuery.noConflict();

$(document).ready(function() {
    btn_scroll_to();                    // Scroll-to button
    ajax_tag_filter();                  // Ajax tag filter in Blog template
});

window.onscroll = function() {
	stickyHeader()
};

var header = document.getElementById("masthead");
var sticky = header.offsetTop;

function stickyHeader() {
  if (window.pageYOffset > sticky) {
    header.classList.add("sticky-top");
  } else {
    header.classList.remove("sticky-top");
  }
}

function ajax_tag_filter() {
    $('.btn-tag-filter').click(function() {
        $('.row-posts').addClass('loading');

        $.ajax({
            type: 'POST',
            dataType: 'json',
            url: site_object.ajaxurl,
            data: {
                action: 'ajax_tag_filter',
                tag_id: $(this).data('tag'),
            },

            success: function(results) {
                $('.row-posts').removeClass('loading');
                $('.row-posts').html('');
                $('.row-posts').html(results.html);
                $('.load-more-container').html('');
                $('.load-more-container').html(results.load_more_html);
            }
        });
    });
}

function btn_scroll_to() {
    $('.btn-scroll-to').click(function() {
        $('html, body').animate({scrollTop:0}, 'slow');
        return false;
    });

    $(window).scroll(function () {
        if ($(window).scrollTop() > 50) {
            $('.btn-scroll-to-top').fadeIn(300);
        } else {
            $('.btn-scroll-to-top').fadeOut(300);
        }
    });
}