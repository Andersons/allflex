<?php
/**
 * The template for displaying archive pages
 *
 * @link https://developer.wordpress.org/themes/basics/template-hierarchy/
 *
 * @package allflex
 */

get_header();
?>

	<main id="primary" class="site-main">
		<section class="section-hero-slider">
			<div class="one-image" style="background-image:url(<?php the_post_thumbnail_url(); ?>)">
				<div class="container d-flex flex-column align-items-center justify-content-center">
					<div class="row align-items-center">
						<header class="page-header">
							<?php
							the_archive_title( '<h1 class="page-title">', '</h1>' );
							the_archive_description( '<div class="archive-description">', '</div>' );
							?>
						</header><!-- .page-header -->
					</div>
				</div>
			</div>
			<div class="scroll-to-bottom">
				<button class="btn-scroll-to btn-scroll-to-bottom" data-scrollto=".section-our-partners" aria-label="Scroll to next section" title="Scroll to next section">
					<span class="fa fa-chevron-down" aria-hidden="true"></span>
				</button>
			</div>
		</section>
		<div class="container">
			<?php
				if ( function_exists('yoast_breadcrumb') ) {
				  yoast_breadcrumb( '<p id="breadcrumbs">','</p>' );
				}
			?>
			<?php if ( have_posts() ) : ?>
				<div class="row row-posts">
					<?php
					/* Start the Loop */
					while ( have_posts() ) :
						the_post(); ?>
							<div class="col-lg-4 col-md-6 blog-item blog-item-post">
							    <a href="<?php the_permalink(); ?>">
							       	<div class="post-thumbnail">
							       			<?php the_post_thumbnail( 'medium' ); ?>
					            		<footer class="entry-footer">
					            			<h4 class="entry-title"><?php the_title(); ?></h4>
					            			<p class="entry-excerpt">
					            				<?php the_excerpt(); ?>
					            			</p>
					            			<span class="btn btn-blue-text">
											Lasīt vairāk</span>
					            		</footer>
					            	</div>
					            </a>
				       		</div>
					<?php endwhile; ?>
				</div>
		
		<?php else :

				get_template_part( 'template-parts/content', 'none' );

			endif;
			?>
		</div> <!-- .container -->
	</main><!-- #main -->

<?php
get_footer();
