<?php
/**
 * The header for our theme
 *
 * This is the template that displays all of the <head> section and everything up until <div id="content">
 *
 * @link https://developer.wordpress.org/themes/basics/template-files/#template-partials
 *
 * @package allflex
 */

?>
<!doctype html>
<html <?php language_attributes(); ?>>
<head>
	<meta charset="<?php bloginfo( 'charset' ); ?>">
	<meta name="viewport" content="width=device-width, initial-scale=1">
	<link rel="profile" href="https://gmpg.org/xfn/11">
	<link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.4.1/css/bootstrap.min.css" integrity="sha384-Vkoo8x4CGsO3+Hhxv8T/Q5PaXtkKtu6ug5TOeNV6gBiFeWPGFN9MuhOf23Q9Ifjh" crossorigin="anonymous">
	<link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.7.0/css/font-awesome.min.css">
	<?php wp_head(); ?>
</head>

<body <?php body_class(); ?>>
<?php wp_body_open(); ?>
<div id="page" class="site">
	<a class="skip-link screen-reader-text" href="#primary"><?php esc_html_e( 'Skip to content', 'allflex' ); ?></a>
	<div class="top-strip">
		<div class="container">
			<span>Oficiālais izplatītājs Latvijā <a href="http://futurefarm.lv" target="_blank">FutureFarm</a></span>
		</div>
	</div>
	<header id="masthead" class="site-header">
		<div class="container">
			<div class="d-flex justify-content-between">
				<div class="site-branding">
					<?php
					the_custom_logo();
					if ( is_front_page() && is_home() ) :
						?>
						<h1 class="site-title"><a href="<?php echo esc_url( home_url( '/' ) ); ?>" rel="home"><?php bloginfo( 'name' ); ?></a></h1>
						<?php
					else :
						?>
						<p class="site-title"><a href="<?php echo esc_url( home_url( '/' ) ); ?>" rel="home"><?php bloginfo( 'name' ); ?></a></p>
						<?php
					endif;
					$allflex_description = get_bloginfo( 'description', 'display' );
					if ( $allflex_description || is_customize_preview() ) :
						?>
						<p class="site-description"><?php echo $allflex_description; // phpcs:ignore WordPress.Security.EscapeOutput.OutputNotEscaped ?></p>
					<?php endif; ?>
				</div><!-- .site-branding -->

				<nav id="site-navigation" class="main-navigation">
					<button class="menu-toggle" aria-controls="primary-menu" aria-expanded="false">
						<span class="mn-icon">
							<span class="mn-line"></span>
							<span class="mn-line"></span>
							<span class="mn-line"></span>
							<span class="mn-line"></span>
						</span>
					</button>
					<?php
					wp_nav_menu(
						array(
							'theme_location' => 'menu-1',
							'menu_id'        => 'primary-menu',
						)
					);
					?>
				</nav><!-- #site-navigation -->
			</div>
		</div>
	</header><!-- #masthead -->
		<?php if (is_page() && has_post_thumbnail( $post->ID ) && !is_page_template( 'our-story.php' ) ){ ?>
		<section class="section-hero-slider">
			<div class="one-image" style="background-image:url(<?php the_post_thumbnail_url(); ?>)">
				<div class="container d-flex flex-column align-items-center justify-content-center">
					<div class="row align-items-center">
						<?php 
						$left_col = get_field('left_column');
						$right_col = get_field('right_column');
						 if ($left_col && $right_col) { ?>
						 	<div class="col-lg-6">
								<?php the_field('left_column'); ?>	
							</div>
							<div class="col-lg-6">
								<?php the_field('right_column'); ?>
							</div>
						<?php } else { ?>
							<div class="col-md-12 px-5">
								<?php if ($left_col) {
									the_field('left_column');
								} else {
									the_field('right_column');
								} ?>
							</div>
					<?php	} ?>	
					</div>
				</div>
				<div class="scroll-to-bottom">
				<button class="btn-scroll-to btn-scroll-to-bottom" data-scrollto=".section-our-partners" aria-label="Scroll to next section" title="Scroll to next section">
					<span class="fa fa-chevron-down" aria-hidden="true"></span>
				</button>
			</div>
			</div>
		</section>
	<?php } ?>