<?php 
/** 
* Template Name: Our Clients 
* The template for displaying Solutions page.
*
*
*@package allflex
*/ 
get_header();
?>

<main id="primary" class="site-main">
	<div class="container py-5">
		<?php
		$the_query = new WP_Query( array(
    'post_type' => 'klienti'
) );

while ( $the_query->have_posts() ) :
    $the_query->the_post(); ?>
    <div class="col-lg-4 col-md-6 blog-item blog-item-post">
        <a href="<?php the_permalink(); ?>">
            <div class="post-thumbnail">
                    <?php the_post_thumbnail( 'medium' ); ?>
                <footer class="entry-footer">
                    <h4 class="entry-title"><?php the_title(); ?></h4>
                        <?php the_excerpt(); ?>
                    <span class="btn btn-blue-text">
                    Lasīt vairāk</span>
                </footer>
            </div>
        </a>
    </div>
<?php endwhile;

/* Restore original Post Data 
 * NB: Because we are using new WP_Query we aren't stomping on the 
 * original $wp_query and it does not need to be reset.
*/
wp_reset_postdata(); ?>
</div>
	</main><!-- #main -->

<?php
get_footer();