<?php
/**
 * The sidebar containing the main widget area
 *
 * @link https://developer.wordpress.org/themes/basics/template-files/#template-partials
 *
 * @package allflex
 */

if ( ! is_active_sidebar( 'sidebar-1' ) && ! is_active_sidebar( 'soc-sidebar' ) ) {
	return;
}
?>

<aside class="d-flex">
	<?php dynamic_sidebar( 'sidebar-1' ); ?>
</aside>
<aside class="d-flex">
	<?php dynamic_sidebar( 'soc-sidebar' ); ?>
</aside>