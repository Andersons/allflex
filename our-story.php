<?php 
/** 
* Template Name: Our Story page 
* The template for displaying Our story page.
*
*
*@package allflex
*/ 
get_header();
?>

<main id="primary" class="site-main">
	<section class="section-hero-slider hero-half">
				<div class="half-bg">
				   <div class="col-md-5 bg-white"></div>
					<div class="half-image col-md-7" style="background-image:url(<?php the_post_thumbnail_url(); ?>)"></div>
				</div>
				<div class="container">
					<div class="row">
						<div class="col-md-4">
						<?php the_field('header_text'); ?>
						</div>
					</div>
				</div>	
			
		
			<div class="scroll-to-bottom">
				<button class="btn-scroll-to btn-scroll-to-bottom" data-scrollto=".section-our-partners" aria-label="Scroll to next section" title="Scroll to next section">
					<span class="fa fa-chevron-down" aria-hidden="true"></span>
				</button>
			</div>
		</section>
		<?php
		while ( have_posts() ) :
			the_post(); ?>
				<section class="section-textual">
					<div class="container">
						<?php
							if ( function_exists('yoast_breadcrumb') ) {
							  yoast_breadcrumb( '<p id="breadcrumbs">','</p>' );
							}
						?>
						<div class="row">
							<div class="col-lg-6">
								<header class="entry-header">
									<h2 class="entry-title"><?php the_title(); ?></h2>
									<div class="entry-text"><?php the_field('main_text'); ?></div>
								</header>
								<?php if (have_rows('solution_images')): ?>
									<div class="images">
										<?php while (have_rows('solution_images')) : the_row(); ?>	
											<div class="col-md-6 col-image">
												<figure class="entry-image">
													<img src="<?php the_sub_field('solution_image') ?>" alt="">
												</figure>
											</div>
										<?php endwhile; ?>
									</div>
								<?php endif; ?>
							</div>
							<div class="col-lg-6">
								<div class="entry-content">
									<?php
									the_content();

									wp_link_pages(
										array(
											'before' => '<div class="page-links">' . esc_html__( 'Pages:', 'allflex' ),
											'after'  => '</div>',
										)
									);
									?>
								</div>
							</div>
						</div>
					</div>
				</section><!-- .section-textual -->
			
			<?php if ( get_field('big_banner') ): ?>
				<section class="bg-white big-banner">
					<div class="container">
						<h1>Mūsu mantojums</h1>
						<?php the_field('big_banner'); ?>
					</div>
				</section><!-- .section-video-modal -->
			<?php endif; ?>
		<?php endwhile; // End of the loop.
		?>
	</main><!-- #main -->

<?php
get_footer();