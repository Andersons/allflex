<?php 
/** 
* Template Name: Front Page 
* The template for displaying home page.
*
*
*@package allflex
*/ 
get_header();
?>

<main id="primary" class="site-main">
		<?php
		while ( have_posts() ) :
			the_post(); ?>

				<section class="entry-content">
					<div class="container">
						<?php
						the_content();

						wp_link_pages(
							array(
								'before' => '<div class="page-links">' . esc_html__( 'Pages:', 'allflex' ),
								'after'  => '</div>',
							)
						);
						?>
					</div>
				</section><!-- .entry-content -->
				<?php if (have_rows('cubes_items')): ?>
					<section class="section-cubes parallax-item">
						<div class="container">	
							<div class="row justify-content-center">
							<?php while (have_rows('cubes_items')) : the_row(); ?>		                
								<div class="col-lg-4">
									<div class="item">
										<div class="entry-icon">	
											<figure class="entry-image">
												<img src="<?php the_sub_field('cube_icon'); ?>" alt="cube icon">
											</figure>
										</div>
										<h3 class="entry-title title-purple">
											<?php the_sub_field('cube_title'); ?>
										</h3>
										<div class="entry-text">
											<?php the_sub_field('cube_text'); ?>
										</div>
									</div>
								</div>
							<?php endwhile; ?>
							</div>
						</div>
					</section>
				<?php endif; ?>
				<?php if (have_rows('solutions')): ?>
					<section class="section-solutions">
						<div class="container">	
							<h2 class="entry-title after-blue">Mūsu risinājumi</h2>
							<div class="row entry-qs-slider entry-qs-slider-solutions">
			   					<?php while (have_rows('solutions')) : the_row(); ?>	
									<div class="col-md-3 col-sm-6 slide">
										<a href="<?php the_sub_field('solution_link'); ?>">
											<div class="item">
												<figure class="entry-image">
													<img src="<?php the_sub_field('solution_image'); ?>" alt="solution image">
												</figure>				
												<footer class="entry-footer">
													<h3 class="entry-title"><?php the_sub_field('solution_title'); ?></h3>
													<div class="entry-text">
														<?php the_sub_field('solution_text'); ?>
													</div>
													<span class="btn btn-blue-text">Uzzināt vairāk</span>
												</footer>
											</div>
										</a>
									</div>
							  	<?php endwhile; ?>
							</div>
						</div>
					</section>
				<?php endif; ?>
			
		<?php endwhile; // End of the loop.
		?>
	</main><!-- #main -->

<?php
get_footer();