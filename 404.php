<?php
/**
 * The template for displaying 404 pages (not found)
 *
 * @link https://codex.wordpress.org/Creating_an_Error_404_Page
 *
 * @package allflex
 */

get_header();
?>

	<main id="primary" class="site-main">

		<section class="error-404 section-hero-slider not-found ">

			<div class="container one-image d-flex justify-content-center align-items-center flex-column">
				<header class="page-header">
					<h1 class="display-1">OOOPS!</h1>
				</header><!-- .page-header -->

				<div class="page-content text-center">
					<h4>Šī lapa netika atrasta. <br> Pamēģini izmantot meklētāju:</h4>
					<p>
						<?php
						get_search_form();

						
						?>
					</p>
				</div><!-- .page-content -->
			</div><!-- .container -->
		</section><!-- .error-404 -->

	</main><!-- #main -->

<?php
get_footer();
