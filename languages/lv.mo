��          �      L      �     �  =   �  W     =   l     �     �     �  !   �  %   �          "  	   1     ;  \   R     �  )   �     �     �  �       �  ;   �  J   ,     w  !   �     �     �  #   �      �               +     4  R   N     �  )   �  
   �     �               
                                            	                                         Comments are closed. Continue reading<span class="screen-reader-text"> "%s"</span> It seems we can&rsquo;t find what you&rsquo;re looking for. Perhaps searching can help. Leave a Comment<span class="screen-reader-text"> on %s</span> Most Used Categories Next: Nothing Found One thought on &ldquo;%1$s&rdquo; Oops! That page can&rsquo;t be found. Pages: Posted in %1$s Previous: Search Results for: %s Sorry, but nothing matched your search terms. Please try again with some different keywords. Tagged %1$s Try looking in the monthly archives. %1$s post authorby %s post datePosted on %s Project-Id-Version: _s 1.0.0
Report-Msgid-Bugs-To: https://wordpress.org/support/theme/_s
Language-Team: 
MIME-Version: 1.0
Content-Type: text/plain; charset=UTF-8
Content-Transfer-Encoding: 8bit
POT-Creation-Date: 2020-04-17T21:03:15+00:00
PO-Revision-Date: 2020-05-07 23:05+0300
X-Generator: Poedit 2.2.3
X-Domain: _s
Last-Translator: 
Plural-Forms: nplurals=3; plural=(n%10==0 || (n%100>=11 && n%100<=19) ? 0 : n%10==1 && n%100!=11 ? 1 : 2);
Language: lv_LV
 Komentāri slēgti. Lasīt tālāk<span class="screen-reader-text"> "%s"</span> Izskatās, ka neizdodas atrast meklēto. Varbūt meklētājs palīdzēs... Uzrakstiet savas domas Visbiežāk lietotās kategorijas Tālāk Nekas netika atrasts Viens komentārs &ldquo;%1$s&rdquo; OOOPs! Šī lapa netika atrasta. Lapas: Kategorijā: %1$s Atpakaļ Meklējuma rezultāti: %s Diemžēl nekas neatbilde meklējumiem. Pamēģiniet ar citiem atslēgas vārdiem. Birkas: %1$s Pamēģini meklēt mēneša arhīvos %1$s autors: %s %s 